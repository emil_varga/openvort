/*******************************************************************************
 * Copyright (C) 2018 Emil Varga <varga.emil@gmail.com>
 * 
 * This file is part of OpenVort
 * 
 * OpenVort is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>


#include "vortex_utils.h"
#include "util.h"
#include "vortex_constants.h"

struct vec3d perpendicular(const struct vec3d *dir)
{
  struct vec3d a = vec3(1,0,0);
  struct vec3d b = vec3(0,1,0);
  struct vec3d res;

  vec3_mul(&res, dir, -vec3_dot(&a, dir));
  vec3_add(&res, &res, &a);

  if(vec3_d(&res) > 1e-8)
    {
      vec3_normalize(&res);
      return res;
    }

  vec3_mul(&res, dir, -vec3_dot(&b, dir));
  vec3_add(&res, &res, &b);
  vec3_normalize(&res);
  return res;
}

/*
 * Primitive shapes
 */

void add_circle(struct tangle_state *tangle,
		struct vec3d *center, struct vec3d *dir, double r,
		int Npoints)
{
  if(Npoints == 0)
      return;

  if(num_free_points(tangle) < Npoints)
    {
      expand_tangle(tangle, tangle->N + Npoints);
    }

  struct vec3d u, v, p, ptmp;
  int curr_point, last_point, first_point;
  struct vec3d zdir = perpendicular(dir);
  
  vec3_cross(&u, &zdir, dir);
  vec3_cross(&v, dir, &u);

  vec3_normalize(&u);
  vec3_normalize(&v);

  first_point = -1;
  curr_point = -1;

  for(int k=0; k<Npoints; ++k)
    {
      double phi = 2*M_PI/Npoints * k;
      double c = cos(phi);
      double s = sin(phi);
      vec3_mul(&p, &u, r*c);
      vec3_mul(&ptmp, &v, r*s);

      vec3_add(&p, &p, &ptmp);
      vec3_add(&p, &p, center);
      curr_point = get_tangle_next_free(tangle);

      tangle->status[curr_point].status = FREE;
      if(first_point < 0)
	first_point = curr_point;

      tangle->vnodes[curr_point] = p;
      if(curr_point != first_point)
	{
	  tangle->connections[curr_point].reverse = last_point;
	  tangle->connections[last_point].forward = curr_point;
	}

      last_point = curr_point;
    }
  tangle->connections[curr_point].forward  = first_point;
  tangle->connections[first_point].reverse = curr_point;
}

void add_wall_circle(struct tangle_state *tangle, struct vec3d *center, struct vec3d *dir, double r, int Npoints)
{
  //TODO
  return;
}

void add_line_KW(struct tangle_state *tangle, double x, double y, int direction, int points,
                int k_KW, double r_KW)
{
  double zmin = tangle->box.bottom_left_back.p[2];
  double zmax = tangle->box.top_right_front.p[2];
  double dz = (zmax - zmin) / points;
  double z;

  double zstart = direction > 0 ? zmin : zmax;
  double zend = direction > 0 ? zmax : zmin;
  double k = M_PI/dz*k_KW; //number of half-waves

  struct vec3d s = vec3(x + r_KW*cos(k*zstart), y + r_KW*sin(k*zstart), zstart);
  struct vec3d sp = vec3(-k*r_KW*sin(k*zstart), k*r_KW*cos(k*zstart), direction); //tangent
  struct vec3d spp = vec3(-k*k*r_KW*cos(k*zstart), -k*k*r_KW*sin(k*zstart), 0); //normal

  const double s_mag = vec3_d(&sp); //the magnitude of the derivative doesn't change, only its direction
  vec3_mul(&sp, &sp, 1/s_mag);
  vec3_mul(&spp, &spp, 1/s_mag);


  int new_pt = get_tangle_next_free(tangle);
  const int first_pt = new_pt;
  const int start_wall = direction > 0 ? Z_L : Z_H;
  const int end_wall = direction > 0 ? Z_H : Z_L;
  int last_pt;
  tangle->vnodes[new_pt] = s;
  tangle->tangents[new_pt] = sp;
  tangle->normals[new_pt] = spp;
  if(tangle->box.wall[start_wall] == WALL_MIRROR) {
    tangle->status[new_pt].status = pin_mode;
    tangle->status[new_pt].pin_wall = start_wall;
    tangle->connections[new_pt].reverse = -1;
  } else if(tangle->box.wall[start_wall] == WALL_PERIODIC) {
    tangle->vnodes[new_pt].p[2] += direction*1e-6; //so that it's not right on the boundary
    tangle->status[new_pt].status = FREE;
    tangle->status[new_pt].pin_wall = NOT_A_FACE;
  } else {
    error("only walls or periodic conditions for add_line");
  }

  for(int k=1; k<points-1; ++k) {
    last_pt = new_pt;
    new_pt = get_tangle_next_free(tangle);
    z = zstart + direction*k*dz;

    s = vec3(x + r_KW*cos(k*z), y + r_KW*sin(k*z), z);
    sp = vec3(-k*r_KW*sin(k*z)/s_mag, 
               k*r_KW*cos(k*z)/s_mag,
               direction/s_mag); //tangent
    spp = vec3(-k*k*r_KW*cos(k*z)/s_mag,
               -k*k*r_KW*sin(k*z)/s_mag,
               0); //normal

    tangle->vnodes[new_pt] = s;
    tangle->tangents[new_pt] = sp;
    tangle->normals[new_pt] = spp;
    tangle->status[new_pt].status = FREE;
    tangle->status[new_pt].pin_wall = NOT_A_FACE;
    tangle->connections[new_pt].reverse = last_pt;
    tangle->connections[last_pt].forward = new_pt;
  }

  z = zend;
  s = vec3(x + r_KW*cos(k*z), y + r_KW*sin(k*z), z);
  sp = vec3(-k*r_KW*sin(k*z)/s_mag, 
              k*r_KW*cos(k*z)/s_mag,
              direction/s_mag); //tangent
  spp = vec3(-k*k*r_KW*cos(k*z)/s_mag,
              -k*k*r_KW*sin(k*z)/s_mag,
              0); //normal
  last_pt = new_pt;
  if(tangle->box.wall[end_wall] == WALL_MIRROR) {
    new_pt = get_tangle_next_free(tangle);
    tangle->vnodes[new_pt] = s;
    tangle->tangents[new_pt] = sp;
    tangle->normals[new_pt] = spp;
    tangle->status[new_pt].status = pin_mode;
    tangle->status[new_pt].pin_wall = end_wall;
    tangle->connections[new_pt].reverse = last_pt;
    tangle->connections[new_pt].forward = -1;
    tangle->connections[last_pt].forward = new_pt;
  } else if (tangle->box.wall[end_wall] == WALL_PERIODIC) {
    tangle->connections[new_pt].forward = first_pt;
    tangle->connections[first_pt].reverse = new_pt;
  } else {
    error("only walls or periodic conditions for add_line");
  }
}

void add_line(struct tangle_state *tangle, double x, double y, int direction, int points)
{
  add_line_KW(tangle, x, y, direction, points, 0, 0);
}

/*
 * File I/O
 */

void write_vector(FILE *stream, struct vec3d *v)
{
  fprintf(stream, "%.15g\t%.15g\t%.15g",
	  v->p[0], v->p[1], v->p[2]);
}

void save_point(FILE *stream, int vort_idx,
		const struct tangle_state *tangle, int i)
{
  fprintf(stream, "%d\t", vort_idx);
  write_vector(stream, tangle->vnodes + i);
  fprintf(stream, "\t");
  write_vector(stream, tangle->vels + i);
  fprintf(stream, "\t");
  write_vector(stream, tangle->tangents + i);
  fprintf(stream, "\t");
  write_vector(stream, tangle->normals + i);
  fprintf(stream, "\t%d\t%d\t%d\t%d\t%d\t%.15g",
    i,
	  tangle->connections[i].reverse,
	  tangle->connections[i].forward,
	  tangle->status[i].status,
	  tangle->status[i].pin_wall,
    tangle->dxi[i]);
  fprintf(stream, "\n");
}

int load_point(FILE *stream, struct tangle_state *tangle, int idx)
{
  int vidx;
  struct vec3d *pos     = &tangle->vnodes[idx];
  struct vec3d *vel     = &tangle->vels[idx];
  struct vec3d *tangent = &tangle->tangents[idx];
  struct vec3d *normal  = &tangle->normals[idx];
  int ret = fscanf(stream, "%d\t%lg\t%lg\t%lg\t%lg\t%lg\t%lg\t%lg\t%lg\t%lg\t%lg\t%lg\t%lg",
	   &vidx,
	   pos->p,     pos->p+1,     pos->p+2,
	   vel->p,     vel->p+1,     vel->p+2,
	   tangent->p, tangent->p+1, tangent->p+2,
	   normal->p,  normal->p+1,  normal->p+2);
  if(ret == EOF)
    return -1;

  tangle->status[idx].status = FREE;

  return vidx;
}

void save_tangle(const char *filename, struct tangle_state *tangle)
{
  int vortex_idx = 0;
  int *visited = (int *)calloc(tangle->N, sizeof(int));
  FILE *stream = fopen(filename, "w");

  if(!stream) {
      fprintf(stderr, "Can't open file %s\n", filename);
      return;
  }

  for(int k=0; k < tangle->N; ++k) {
    if(!visited[k])	{
	    if(tangle->status[k].status == EMPTY) {
	      visited[k] = 1;
	      continue;
	    }
	  
	    int first = k;
	    int curr  = k;

	    while(tangle->connections[curr].forward != first) {
	      save_point(stream, vortex_idx, tangle, curr);
	      visited[curr] = 1;
	      curr = tangle->connections[curr].forward;
	      if(curr < 0) {
		      curr = tangle->connections[first].reverse;
		      while(curr > 0 && tangle->connections[curr].reverse > 0) {
            save_point(stream, vortex_idx, tangle, curr);
            visited[curr] = 1;
            curr = tangle->connections[curr].reverse;
		      }
		      break;
		    }
      }
      
	    if(curr > 0) {
	      save_point(stream, vortex_idx, tangle, curr);
	      visited[curr] = 1;
	    }
	    vortex_idx++;
	  }
  }
  
  printf("saving frame\n");
  free(visited);
  fclose(stream);
}

int load_tangle(const char *filename, struct tangle_state *tangle)
{
  FILE *file = fopen(filename, "r");
  int vidx = 0; //vortex index
  int current_vidx; //index of vortex we are loading now
  int pidx = 0; //index of position to store a loaded point
  int pidx_start = 0; //where does the vortex begin
  while((current_vidx = load_point(file, tangle, pidx)) >= 0) {
    tangle->connections[pidx].reverse = pidx - 1;
    tangle->connections[pidx].forward = pidx + 1;

    if(current_vidx != vidx) {
      //we are on a new vortex
      //stitch together the vortex we finished
      tangle->connections[pidx_start].reverse = pidx-1;
      tangle->connections[pidx-1].forward = pidx_start;

      //move on to the next
      vidx++;
      pidx_start = pidx;
      if(current_vidx != vidx)
	      return -1; //something's wrong
	  }

    pidx++;
    if(pidx > tangle->N - 1)
      expand_tangle(tangle, 2*tangle->N);
  }

  //stitch the last vortex together because the inner if never ran
  tangle->connections[pidx_start].reverse = pidx-1;
  tangle->connections[pidx-1].forward = pidx_start;

  return 0;
}

/*
 * Debugging utilities
 */

int check_loop(const struct tangle_state *tangle, int *visited, int k);
int check_integrity(const struct tangle_state *tangle)
{
  int *visited = calloc(tangle->N, sizeof(int));
  int errors = 0;

  for(int k=0; k < tangle->N; ++k) {
    int next = tangle->connections[k].forward;
    int prev = tangle->connections[k].reverse;
    if(next >= 0 && tangle->status[k].status == FREE)	{
      if(k != tangle->connections[next].reverse)
        error("Forward connection broken %d %d %d\n", k, next, 
              tangle->connections[next].reverse);
	  }
    if(prev >= 0 && tangle->status[k].status == FREE) {
      if( k!= tangle->connections[prev].forward)
        error("Reverse connection broken %d %d %d\n", k, prev,
              tangle->connections[prev].forward);
	  }
    if(!visited[k])	{
	    visited[k] += 1;	  	  
	    errors += check_loop(tangle, visited, k);
	  }
  }

  free(visited);
  return errors;
}

int check_loop(const struct tangle_state *tangle, int *visited, int k)
{
  int errors = 0;
  int rval;

  if((rval = is_empty(tangle, k)) > 0)
    return 0;
  if(rval < 0)
    return rval;
  
  int j = tangle->connections[k].forward;
  int total = 0;
  while(j != k && total < tangle->N)
    {
      if(visited[j])
	{
#ifdef _DEBUG_
	  printf("Ran into point %d again.\n", j);
#endif
	  errors--;
	  break;
	}
      visited[j]++;
      if((rval = is_empty(tangle, j)) > 0)
	{
#ifdef _DEBUG_
	  printf("Connected to an empty point, %d %d.\n",
		 k, j);
#endif
	  errors--;
	  return errors;
	}
      if(tangle->connections[j].forward < 0)
	{
#ifdef _DEBUG_
	  printf("Linked point with -1 forward, %d.\n",
		 j);
#endif
	  errors--;
	  return errors;
	}

      j = tangle->connections[j].forward;

      total++;
    }


  if(j != k && total == tangle->N)
    {
#ifdef _DEBUG_
      printf("Ran through the entire tangle from %d.\n", k);
#endif
      errors--;
    }

  return errors;
}

int is_empty(const struct tangle_state *tangle, int k)
{
  struct neighbour_t nb = tangle->connections[k];

  if(nb.forward == -1 && nb.reverse == -1)
    return 1; //point is empty

  if(nb.forward >= 0 && nb.reverse >= 0)
    return 0; //both links are valid, point is not empty

  //one of the connections is negative, but the point is pinned so it's OK
  if(tangle->status[k].status == PINNED || tangle->status[k].status == PINNED_SLIP)
    return 0;

  //one of the links is >= 0, the other is ==-1 and it is not pinned
  //point is corrupted
  #ifdef _DEBUG_
  printf("Corrupted links in point %d (%d, %d)\n",
	 k,
	 tangle->connections[k].forward,
	 tangle->connections[k].reverse);
  #endif
  return -1;
}

/*
 * Wall-related stuff
 */

double wall_dist(const struct tangle_state *tangle, int k, boundary_faces wall)
{
  /*
   * Returns the distance of point k to the specified wall.
   */
  int idx[6];
  idx[X_L] = idx[X_H] = 0;
  idx[Y_L] = idx[Y_H] = 1;
  idx[Z_L] = idx[Z_H] = 2;
  switch(wall)
  {
    case X_L:
    case Y_L:
    case Z_L:
      return tangle->vnodes[k].p[idx[wall]] - tangle->box.bottom_left_back.p[idx[wall]];

    case X_H:
    case Y_H:
    case Z_H:
      return tangle->box.top_right_front.p[idx[wall]] - tangle->vnodes[k].p[idx[wall]];

    default:
      error("wall_dist: unknown wall index %d\n", wall);
  }
  return -1;
}

void clip_at_wall(struct tangle_state *tangle)
{
  /*
   * Clips the tangle at the z-walls
   */
  //limits for unconstrained walls
  double llimit = -INFINITY;
  double ulimit = INFINITY;

  if(tangle->box.wall[Z_L] == WALL_MIRROR)
    llimit = tangle->box.bottom_left_back.p[2];
  if(tangle->box.wall[Z_H] == WALL_MIRROR)
    ulimit = tangle->box.top_right_front.p[2];

  enum POINT_STATES {
        OK=0,
        KILL,         //point to be clipped
        EDGE_FORWARD_L, //last point above the lower wall
        EDGE_REVERSE_L,  //first point above the lower wall
        EDGE_FORWARD_H, //last point below the upper wall
        EDGE_REVERSE_H  //first point below the upper wall
      };

  //recalculate is used as a tag for points to be clipped
  for(int k=0; k<tangle->N; ++k)
    tangle->recalculate[k] = OK;

  //first find all the points to be clipped
  for(int kk=0; kk<tangle->N; ++kk)
    {
      if(tangle->status[kk].status == EMPTY)
	continue;

      double zkk = tangle->vnodes[kk].p[2];
      if(zkk <= llimit || zkk >= ulimit)
	{
	  tangle->recalculate[kk] = KILL;
	  const int forward = tangle->connections[kk].forward;
	  const int reverse = tangle->connections[kk].reverse;

	  double zf = tangle->vnodes[forward].p[2];
	  double zr = tangle->vnodes[reverse].p[2];

	  if(zkk >= ulimit)
	    {
	      if(zf < ulimit)
		tangle->recalculate[forward] = EDGE_REVERSE_H;
	      if(zr < ulimit)
		tangle->recalculate[reverse] = EDGE_FORWARD_H;
	    }
	  if(zkk <= llimit)
	    {
	      if(zf > llimit)
		tangle->recalculate[forward] = EDGE_REVERSE_L;
	      if(zr > llimit)
		tangle->recalculate[reverse] = EDGE_FORWARD_L;
	    }
	}
    }

  //next handle edge points by projecting them on the wall
  //and pinning them on the lower or upper Z-wall
  for(int kk=0; kk < tangle->N; ++kk)
    {
      switch(tangle->recalculate[kk])
      {
	case EDGE_REVERSE_L:
	  tangle->connections[kk].reverse = -1;
	  tangle->vnodes[kk].p[2] = llimit;
	  tangle->status[kk].status = pin_mode;
	  tangle->status[kk].pin_wall = Z_L;
	  break;
	case EDGE_FORWARD_L:
	  tangle->connections[kk].forward = -1;
	  tangle->vnodes[kk].p[2] = llimit;
	  tangle->status[kk].status = pin_mode;
	  tangle->status[kk].pin_wall = Z_L;
	  break;
	case EDGE_REVERSE_H:
	  tangle->connections[kk].reverse = -1;
	  tangle->vnodes[kk].p[2] = ulimit;
	  tangle->status[kk].status = pin_mode;
	  tangle->status[kk].pin_wall = Z_H;
	  break;
	case EDGE_FORWARD_H:
	  tangle->connections[kk].forward = -1;
	  tangle->vnodes[kk].p[2] = ulimit;
	  tangle->status[kk].status = pin_mode;
	  tangle->status[kk].pin_wall = Z_H;
	  break;
	case KILL:
	  tangle->status[kk].status = EMPTY;
	  tangle->status[kk].pin_wall = NOT_A_FACE;
	  tangle->connections[kk].forward = -1;
	  tangle->connections[kk].reverse = -1;
	  break;
	default:
	  break;
      }
    }
}
